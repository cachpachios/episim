import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import Chart from 'vue2-frappe'

Vue.config.productionTip = false

Vue.use(Chart);

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
